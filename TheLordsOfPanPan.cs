using TheLordsOfPanPan.NPCs;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using Terraria;
using Terraria.GameContent.Dyes;
using Terraria.GameContent.UI;
using Terraria.Graphics.Effects;
using Terraria.Graphics.Shaders;
using Terraria.ID;
using Terraria.Localization;
using Terraria.ModLoader;
using Terraria.UI;

namespace TheLordsOfPanPan
{   
    public class TheLordsOfPanPan : Mod
	{
        public static int OldCoinID;
        public TheLordsOfPanPan()
		{
		}
        public override void Load()
        {
        OldCoinID = CustomCurrencyManager.RegisterCurrency( new OldCoinCurrency(ItemType("OldCoin"), 999L));
        }
	}
    
}