﻿using Terraria;
using Terraria.ID;
using Terraria.Localization;
using Terraria.ModLoader;
using ReLogic.Localization.IME;

namespace TheLordsOfPanPan.NPCs
{
    [AutoloadHead]
    public class NonFamiliarLookingMerchant : ModNPC
    {
        public override string Texture {
            get { return "TheLordsOfPanPan/NPCs/NonFamiliarLookingMerchant"; }
        }

        public override string[] AltTextures
        {
            get { return new[] { "TheLordsOfPanPan/NPCs/NonFamiliarLookingMerchant_Alt_1" }; }
        }
        public override bool Autoload(ref string name)
        {
        if (GameCulture.Spanish.IsActive)
            {
            name = ("Mercader de aspecto no familiar");
            }
        if (GameCulture.English.IsActive)
            {
             name = ("Non Familiar Looking Merchant");
            }
        return mod.Properties.Autoload;
        }
        
        public override void SetStaticDefaults()
        {
            Main.npcFrameCount[npc.type] = 25;
            NPCID.Sets.ExtraFramesCount[npc.type] = 9;
            NPCID.Sets.AttackFrameCount[npc.type] = 5;
            NPCID.Sets.DangerDetectRange[npc.type] = 700;
            NPCID.Sets.AttackType[npc.type] = 0;
            NPCID.Sets.AttackTime[npc.type] = 90;
            NPCID.Sets.AttackAverageChance[npc.type] = 30;
            NPCID.Sets.HatOffsetY[npc.type] = 4;
        }

        public override void SetDefaults()
        {
            npc.townNPC = true;
            npc.friendly = true;
            npc.width = 18;
            npc.height = 40;
            npc.aiStyle = 7;
            npc.damage = 12;
            npc.defense = 17;
            npc.lifeMax = 250;
            npc.HitSound = SoundID.NPCHit1;
            npc.DeathSound = SoundID.NPCDeath1;
            npc.knockBackResist = 0.5f;
            animationType = NPCID.ArmsDealer;
        }

        public override bool CanTownNPCSpawn(int numTownNPCs, int money)
        {
            
            for(int k = 0; k < 255; k++)
            {
                Player player = Main.player[k];
                if(!player.active)
                {
                    continue;
                }

                foreach(Item item in player.inventory)
                {
                    if(item.type == mod.ItemType("OldCoin"))
                    {
                        return true;
                    }
                }                
            }
            return false;
        }

        public override string TownNPCName()
        {
            switch(WorldGen.genRand.Next(16))
            {
                case 0:
                    return "Tho'Nan";
                case 1:
                    return "Aph'Suk";
                case 2:
                    return "Zhan'Ik";                
                case 3:
                    return "Ken'Ath";
                case 4:
                    return "Urus";
                case 5:
                    return "Ubot";                
                case 6:
                    return "Ilhae";
                case 7:
                    return "Ag'Han";
                case 8:
                    return "Ka'That";
                case 9:
                    return "Aph'Tau";
                case 10:
                    return "Nott";
                case 11:
                    return "Sos'Sai";
                case 12:
                    return "Tah,Az";
                case 13:
                    return "N'Shael";
                case 14:
                    return "Eon";
                default: 
                    return "Xolv";
            }
        }

        public override string GetChat()
        { 
            int otherNPC = NPC.FindFirstNPC(NPCID.Guide);
            if(otherNPC >= 0 && Main.rand.NextBool(20))
            {
                if (GameCulture.English.IsActive){
                return "I have seen " + Main.npc[otherNPC].GivenName +  " doing some misterious things. Be careful, trust nobody... Except for me of course.";}                if (GameCulture.Spanish.IsActive){
                return "He visto a " + Main.npc[otherNPC].GivenName +  " hacer algunas cosas misteriosas. Ten cuidado, No confies en nadie... Excepto por mi claro.";}    
            }
            switch(Main.rand.Next(15))
            {
            case 0:
                if(GameCulture.Spanish.IsActive){return "El mundo se acabara pronto, no por fuego o hielo, sino por caos y... El vacio. Eso es lo que he visto en mis sueños. Mientras tanto, tienes algo para negociar?";}
                if(GameCulture.English.IsActive){return "The world will end soon, not with fire or ice, but by chaos and... The void. Thats what I've seen in my dreams. In the meantime, do you have some stuff to trade?";}
                else{return "...";}
                    
                case 1:
            if(GameCulture.Spanish.IsActive){return "Las monedas son del pasado, pero ellas pueden mostrarte el futuro... Algo que pueda hacer por ti?";}
            if(GameCulture.English.IsActive){return "The coins are from the past, but they can show the future... Something that I can do you for you?";}
            else{return "...";}
                case 2:
                
                if(GameCulture.English.IsActive)
                {
                    return "Ya'Tul be with you, what you have for me?";}
                if(GameCulture.Spanish.IsActive)
                {
                    return "Ya'Tul sea contigo, que tienes para mi?";}
                else{return "...";}    
                    
                case 3:
                
                if(GameCulture.English.IsActive)
                {
                    return "More coins, more stuff. Better stuff, more coins.";}
                if(GameCulture.Spanish.IsActive)
                {
                    return "Mas monedas, mas cosas. Mejores cosas, mas monedas.";}
                 else{return "...";}   
                case 4:
                
                if(GameCulture.English.IsActive)
                {
                    return "You think I'm crazy? Look at this stuff and tell me if it doesn't makes you crazy... Hey, hello, I didn't saw you, you want something?";}
                if(GameCulture.Spanish.IsActive)
                {
                    return "Tu crees que estoy loco? Mira a estas cosas y dime que no te vuelven loco... Hey, hola no te habia visto, quieres algo?";}
                else{return "...";}
                case 5:
                
                if(GameCulture.English.IsActive)
                {
                    if(!Main.dayTime)
                    {
                    return "⍱⍵'⍭⍲⍳... Sorry, but you must know... You're in danger, buy something, protect youself.";
                    }
                    else
                    {
                    return "⍱⍵'⍭⍲⍳...";
                    }}
                if(GameCulture.Spanish.IsActive)
                {
                    if(!Main.dayTime)
                    {
                    return "⍱⍵'⍭⍲⍳... Lo lamento, pero debes saber... Estas en peligro, compra algo, protegete.";
                    }
                    else
                    {
                    return "⍱⍵'⍭⍲⍳...";
                    }}
                else{return "...";}
                case 6:
                
                if(GameCulture.English.IsActive)
                {
                    if(NPC.downedBoss1)
                    {return "That big demon eye, Is not the only one. You will see that there are worst things. Be prepared, buy more things.";}
                    else{return "You know that there is an eye, a big eye, a demon eye, kill it with this equipment.";}}
                if(GameCulture.Spanish.IsActive)
                {
                    if(NPC.downedBoss1)
                    {return "Ese gran ojo demoniaco, No es el unico. Ya veras que hay peores cosas. Esta preparado, compra mas cosas.";}
                    else{return "Sabes que hay un ojo, un gran ojo, un ojo demoniaco, matalo con este equipamiento.";}}
                else{return "...";}    
                case 7:
                
                if(GameCulture.English.IsActive)
                {
                    if(NPC.downedBoss2){return "Well, while the corrupted monster remains death buy some new equipment.";}
                    else{return "There is a monster in the corruption, buy something and go, kill it.";}}
                if(GameCulture.Spanish.IsActive)
                {
                    if(NPC.downedBoss2){return "Bueno, mientras el monstruo corrupto permanezca muerto compra equipamiento nuevo.";}
                    else{return "Hay un monstruo en la corrupcion, compra algo y ve, matalo.";}}
                else{return "...";}
                case 8:
                
                if(GameCulture.English.IsActive)
                {
                    if(NPC.downedBoss3){return "I remember our... my days of glory, I also killed Skeletron. I mean, not that Skeletron that you just killed... well there are... Is that I'm... JUST BUY SOMETHING!!!";}
                    else{
                    return "You should go to talk that old man in the end of the world. I think that talking with him will summon a giant skeleton... But I don't know the future kid... Just the past... Want something?";}}
                else{return "...";}
                if(GameCulture.Spanish.IsActive)
                {
                    if(NPC.downedBoss3){return "Recuerdo nuestro... mis dias de gloria, yo tambien mate a Skeletron. Me refiero no este Skeletron que acabas de matar... bueno enrealidad hay... Es que yo soy... SOLO COMPRA ALGO!!!";}
                    else{
                    return "Deberias ir y hablar con el hombre viejo al final del mundo. Piendo que hablar con el invocara a un esqueleto gigante... Pero yo no veo el futuro chico... Solo el pasado... Quieres algo?";}}
                else{return "...";}  
                case 9:
                
                if(GameCulture.English.IsActive)
                {
                    if(Main.hardMode){return "Well... The other guide killed himself... Don't blame yourself, you didn't know how the doll worked. It wasn't your fault. I've got new things if that helps.";}
                    else{return "You didn't know it, It wasn't your fault... Hey, hi there... Want to buy something?";}}
                if(GameCulture.Spanish.IsActive)
                {
                    if(Main.hardMode){return "Bueno... El otro guia se quito la vida a si mismo... Para de culparte, tu no sabias como funcionaba la muñeca. No fue tu culpa. Tengo nuevas cosas si eso ayuda.";}
                    else{return "Tu no lo sabias, no fue tu culpa... Hey, hola... Quieres comprar algo?";}}
                 else{return "...";}   
                case 10:
                
                if(GameCulture.English.IsActive)
                {
                    if(NPC.downedMechBoss1 && NPC.downedMechBoss2 && NPC.downedMechBoss3){return "It seems that mechanisms aren't stronger than you. Talking about that, I've got new ancient stuff to make you stronger, want to see?";}
                    else{return "Ya'Tul be with us...";}}
                if(GameCulture.Spanish.IsActive)
                {
                    if(NPC.downedMechBoss1 && NPC.downedMechBoss2 && NPC.downedMechBoss3){return "Parece que los mecanismos no son mas fuertes que tu. Hablando de eso, Tengo nuevas cosas antiguas para hacerte mas fuerte, quieres ver?";}
                    else{return "Ya'Tul sea con nosotros...";}}
                 else{return "...";}   
                case 11:
                
                if(GameCulture.English.IsActive)
                {
                    if(NPC.downedPlantBoss){return "I still remember the smell of her body, the leaves, how beautiful she was... Well, at least the key was useful... want something?";}
                    else{return "Don't you think how cute are plants? Wrong! never trust a plant, be prepared for the attack, even the attack of plants. Want equipment?";}}
                if(GameCulture.Spanish.IsActive)
                {
                    if(NPC.downedPlantBoss){return "Aun recuerdo el olor de su cuerpo, sus hojas, lo bella que era... Bueno, al menos la llave fue util... Quieres algo?";}
                    else{return "No has pensado en lo hermosas que son las plantas? Mal! nunca confies en una planta, esta preparado para el ataque, incluso para el ataque de las plantas. Quieres equipamiento?";}}
                else{return "...";}    
                case 12:
                
                if(GameCulture.English.IsActive)
                {
                    if(NPC.downedGolemBoss){return "You have done too many things for this world. thats why I have new equipment.";}
                    else{return "When you thing I will die? thats right, I wont. Be like me and buy new stuff.";}}
                if(GameCulture.Spanish.IsActive)
                {
                    if(NPC.downedGolemBoss){return "Tu has hecho demasiadas cosas por este mundo. por eso es que tengo equipamiento nuevo.";}
                    else{return "Cuando crees que muera? eso es correcto, no lo hare. Se como yo y compra nuevas cosas.";}}
                else{return "...";}
                case 13:
                
                if(GameCulture.English.IsActive)
                {
                    if(NPC.downedMoonlord){return "I have something to confess... I... I'm you, that why I have this things...";}
                    else{return "Ya'Tul is the answer, that and destroying the evil of this world.";}}
                if(GameCulture.Spanish.IsActive)
                {
                    if(NPC.downedMoonlord){return "Tengo algo que confesar... Yo... Yo soy tu, por eso es que tengo estas cosas...";}
                    else{return "Ya'Tul es la respuesta, eso y destruir la maldad en este mundo.";}}
                else{return "...";}
                case 24:
                    return "";
                case 25:
                
                    return "";
                case 26:
                
                    return "";
                case 27:
                
                    return "";
                default:
                    if(GameCulture.English.IsActive)
                    {return "Ya'Tul will help us defeat the evil in this world. Ya'Tul will help us defeat The void.";}
                    if(GameCulture.Spanish.IsActive)
                    {return "Ya'Tul nos ayudara a derrotar el mal en este mundo. Ya'Tul nos ayudara a derrotar a El vacio.";}
                    else{return "...";}
            }
        }

        public override void SetChatButtons(ref string button, ref string button2)
        {
            
            if (GameCulture.Spanish.IsActive)
            {
            button = ("Mercancia antigua");
            }
            if (GameCulture.English.IsActive)
            {
            button = ("Ancient merchandise");
            }

            
        }

        public override void OnChatButtonClicked(bool firstButton, ref bool shop)
        {
            if(firstButton)
            {
                // This is makes it a shop
                shop = true;
            }
            else
            {Main.npcChatText = "...";
            }
        }

        public override void SetupShop(Chest shop, ref int nextSlot)
        {
            // For every slot, you must have these lines.
            shop.item[nextSlot].SetDefaults(mod.ItemType("OldCoin"));
            shop.item[nextSlot].shopCustomPrice = Item.buyPrice(0, 0, 50, 0);
            nextSlot++;
            // You can have a max of 40?
            
         shop.item[nextSlot].SetDefaults(mod.ItemType("RedCrystal"));
         shop.item[nextSlot].shopCustomPrice = new int?(200); 
        shop.item[nextSlot].shopSpecialCurrency = TheLordsOfPanPan.OldCoinID;          
            nextSlot++;
         shop.item[nextSlot].SetDefaults(mod.ItemType("IronPauldron"));
         shop.item[nextSlot].shopCustomPrice = new int?(150); 
        shop.item[nextSlot].shopSpecialCurrency = TheLordsOfPanPan.OldCoinID;          
            nextSlot++;
        
           
           
        shop.item[nextSlot].SetDefaults(ItemID.DemonScythe);
        shop.item[nextSlot].shopCustomPrice = new int?(2);
        shop.item[nextSlot].shopSpecialCurrency = TheLordsOfPanPan.OldCoinID;
                nextSlot++;
           
            
            nextSlot++;
            // You can also have conditions
            if(Main.moonPhase == 2) // The Phase of the Moon
            {
                shop.item[nextSlot].SetDefaults(ItemID.DemonScythe);
                nextSlot++;
            }
            if(Main.hardMode)
            {
                shop.item[nextSlot].SetDefaults(ItemID.MoonCharm);
                nextSlot++;
            }
            if(Main.LocalPlayer.HasBuff(BuffID.Regeneration)) // If we have a certain buff
            {
                shop.item[nextSlot].SetDefaults(ItemID.RecallPotion);
                nextSlot++;
            }

        }

        public override void NPCLoot()
        {  
            Item.NewItem(npc.getRect(), mod.ItemType("OldCoin"), 100);
        }

        public override void TownNPCAttackStrength(ref int damage, ref float knockback)
        {
            damage = 25;
            knockback = 4f;
        }

        public override void TownNPCAttackCooldown(ref int cooldown, ref int randExtraCooldown)
        {
            cooldown = 30;
            randExtraCooldown = 25;
        }

        public override void TownNPCAttackProj(ref int projType, ref int attackDelay)
        {
            projType = ProjectileID.WaterBolt;
            attackDelay = 1;
        }

        public override void TownNPCAttackProjSpeed(ref float multiplier, ref float gravityCorrection, ref float randomOffset)
        {
            multiplier = 5f;
            randomOffset = 2f;
        }


        // This is for if you want custom spawn conditions
        // This example will check for the TMMCTile and TMMCWall
        public override bool CheckConditions(int left, int right, int top, int bottom)
        {
            return true;
        }

    }
}