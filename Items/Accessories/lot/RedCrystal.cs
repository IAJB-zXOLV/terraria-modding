using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.Localization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLordsOfPanPan.Items.Accessories.lot
{
    public class RedCrystal : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Red Crystal");
            Tooltip.SetDefault(@"A little stone that would make feel stronger even to the strongest one of all.
+15 life"); DisplayName.AddTranslation(GameCulture.Spanish, "Cristal Rojo");
            Tooltip.AddTranslation(GameCulture.Spanish, @"Una pequeña piedra que haria sentir mas fuerte incluso al mas fuerte de todos.
+15 vida");
        }



        public override void SetDefaults()
        {
            item.width = 20;
            item.height = 20;
            item.value = Item.sellPrice(0, 0, 40, 0);
            item.rare = 3;
            item.accessory = true;
        }
        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            player.statLifeMax2 += 15; 
		}
    }
}