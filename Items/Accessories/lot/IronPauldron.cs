using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.Localization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLordsOfPanPan.Items.Accessories.lot
{
    public class IronPauldron : ModItem
    {
        public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Iron Pauldron");
            Tooltip.SetDefault(@"Not only you have more protection, you also look awesome.
+2 defense"); DisplayName.AddTranslation(GameCulture.Spanish, "Hombrera de hierro");
            Tooltip.AddTranslation(GameCulture.Spanish, @"No solo tienes mas proteccion, tambien te ves fabuloso.
+2 defensa");
        }



        public override void SetDefaults()
        {
            item.width = 10;
            item.height = 10;
            item.value = Item.sellPrice(0, 0, 30, 0);
            item.rare = 3;
            item.accessory = true;
        }

        public override void UpdateAccessory(Player player, bool hideVisual)
        {
            player.statDefense += 2; 
		}
    }
}