using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.Localization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheLordsOfPanPan.Items.Misc
{
    public class OldCoin : ModItem
    {
            public override void SetStaticDefaults()
        {
            DisplayName.SetDefault("Ancient Coin");
            Tooltip.SetDefault(@"Who knows where and when did this coins were created.");
DisplayName.AddTranslation(GameCulture.Spanish, "Moneda Antigua");
            Tooltip.AddTranslation(GameCulture.Spanish, @"Quien sabe cuando y donde fueron creadas estas monedas.");
        }
        public override void SetDefaults()
        {
            item.width = 4;
            item.height = 4;
            item.maxStack = 99999;
            item.value = Item.buyPrice(0, 0, 0, 20);
            item.rare = 2;
        }
    }
}