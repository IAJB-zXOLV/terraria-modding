using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria;
using Terraria.ModLoader;

namespace Vaashnur.Buffs
{
    public class MaldicionAntigua : ModBuff
    {
        public override void SetDefaults()
        {
            DisplayName.SetDefault("Maldicion Antigua");
            Description.SetDefault("Sangre! Poder! La mascara exige cosas.");
        }

        public override void Update(Player player, ref int buffIndex)
        {
            player.statLifeMax2 -= 60;
            player.statDefense -= 5;
            player.inferno = true;
        }

    }
}
