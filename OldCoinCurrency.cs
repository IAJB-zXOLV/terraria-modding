using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Terraria;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria.Localization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terraria.GameContent.UI;
using Terraria.Graphics;
using Terraria.UI.Chat;
using Microsoft.Xna.Framework;

namespace TheLordsOfPanPan
{
    public class OldCoinCurrency : CustomCurrencySingleCoin
    {
        public Color OldCoinCurrencyTextColor = Color.Wheat;

		public OldCoinCurrency(int coinItemID, long currencyCap) : base(coinItemID, currencyCap) {
		}

		public override void GetPriceText(string[] lines, ref int currentLine, int price) 
        {
        if(GameCulture.English.IsActive)
        {
        Color color = OldCoinCurrencyTextColor * ((float)Main.mouseTextColor / 255f);
			lines[currentLine++] = string.Format("[c/{0:X2}{1:X2}{2:X2}:{3} {4} {5}]", new object[]
				{
					color.R,
					color.G,
					color.B,
					Language.GetTextValue("LegacyTooltip.50"),
					price,
                    
					"Ancient Coins"
				});
        }
                if(GameCulture.Spanish.IsActive)
        {
        Color color = OldCoinCurrencyTextColor * ((float)Main.mouseTextColor / 255f);
			lines[currentLine++] = string.Format("[c/{0:X2}{1:X2}{2:X2}:{3} {4} {5}]", new object[]
				{
					color.R,
					color.G,
					color.B,
					Language.GetTextValue("LegacyTooltip.50"),
					price,
                    
					"Monedas Antiguas"
				});
        }
		}
    }
}